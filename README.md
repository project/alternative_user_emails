Alternative User Emails
======================

This module stores alternative email addresses for users, in addition to the
single primary email address stored in the user's mail field.

This can be useful for preventing duplicate users being created for people 
who have multiple email addresses, as most of us do.

## What it does

- A multi-value field called 'alternative_user_emails' is added to the user 
entity type.
- When the primary email is changed, the old email is automatically added to 
the alternatives.
- User entity queries are altered so that queries  that filter by the mail 
field also filter by the alternatives.
- Validation ensures that mails and alternatives are unique to a user.

## How to use

No configuration is needed or available.

## Recommended modules
- [Read-only widget](https://www.drupal.org/project/readonly_field_widget) is 
useful if you wish to show the alternative emails on a user form, but not allow 
them to be edited.

## Related modules
- [Multiple Email Addresses](https://www.drupal.org/project/multiple_email) is 
a much more ambitious and feature-rich approach to allowing multiple email 
addresses for users. However, it is only available for Drupal 7.

## Sponsors
- Initial development: [Awakened Heart Sangha](https://ahs.org.uk)

## Current maintainers:
- Jonathan Shaw [(jonathanshaw)](https://www.drupal.org/u/jonathanshaw)
- Patrick Kenny [(ptmkenny)](https://www.drupal.org/u/ptmkenny)
