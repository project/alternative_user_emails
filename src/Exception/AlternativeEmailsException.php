<?php

declare(strict_types=1);

namespace Drupal\alternative_user_emails\Exception;

/**
 * Exceptions for the alternative_user_emails module.
 */
class AlternativeEmailsException extends \Exception {
}
