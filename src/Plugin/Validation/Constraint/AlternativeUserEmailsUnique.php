<?php

declare(strict_types=1);

namespace Drupal\alternative_user_emails\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if a user's alternative emails are unique, considering primaries also.
 *
 * @Constraint(
 *   id = "AlternativeUserEmailsUnique",
 *   label = @Translation("Alternative user emails unique", context = "Validation"),
 *   type = "string",
 * )
 */
class AlternativeUserEmailsUnique extends Constraint {

  /**
   * The message that will be shown if the value is not unique.
   *
   * @var string
   */
  public string $notUnique = 'The email address %value is already associated with another user account.';

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function validatedBy(): string {
    return AlternativeUserEmailsValidator::class;
  }

}
