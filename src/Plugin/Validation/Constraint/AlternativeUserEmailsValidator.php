<?php

declare(strict_types=1);

namespace Drupal\alternative_user_emails\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\alternative_user_emails\Exception\AlternativeEmailsException;
use Drupal\user\UserInterface;
use Drupal\user\UserStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates that alternative_emails_field emails are unique on the site.
 */
class AlternativeUserEmailsValidator extends ConstraintValidator implements ContainerInjectionInterface {

  final public function __construct(
    protected UserStorageInterface $userStorage,
  ) {}

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public static function create(ContainerInterface $container): AlternativeUserEmailsValidator {
    return new static($container->get('entity_type.manager')->getStorage('user'));
  }

  /**
   * The fields on which this validator can operate.
   */
  protected const VALID_FIELDS = ['alternative_user_emails', 'mail'];

  /**
   * Checks whether this validator can process a field.
   *
   * These fields contain email addresses, and email addresses
   * must be unique across all fields.
   *
   * @param string $field_name
   *   The name of the field to check.
   *
   * @return bool
   *   TRUE if the field can be validated by this validator.
   */
  protected function canFieldBeProcessed(string $field_name): bool {
    return in_array($field_name, self::VALID_FIELDS, TRUE);
  }

  /**
   * {@inheritdoc}
   */
  #[\Override]
  public function validate(mixed $value, Constraint $constraint): void {
    if ($value->isEmpty()) {
      return;
    }

    $user = $value->getEntity();
    if (!($user instanceof UserInterface)) {
      throw new AlternativeEmailsException("AlternativeUserEmailsValidator can only process user entities!");
    }
    if (!$this->canFieldBeProcessed($value->getFieldDefinition()->getName())) {
      throw new AlternativeEmailsException("AlternativeUserEmailsValidator can only process mail fields!");
    }

    $uid = $user->id();
    $uid_key = $user->getEntityType()->getKey('id');

    $is_unique = TRUE;
    $bad_email = '';
    // Validate each alternative email given.
    foreach ($value as $item) {
      // Query for other users with the same email.
      $query = $this->userStorage->getQuery();
      $query->accessCheck(FALSE);

      // Exclude the current user.
      // Using isset() instead of !empty() as 0 and '0' are valid ID values for
      // user types using string IDs.
      if (isset($uid)) {
        $query->condition($uid_key, $uid, '<>');
      }

      // Check the primary and alternative emails on other users.
      $or = $query->orConditionGroup()
        ->condition('mail', $item->value)
        ->condition('alternative_user_emails', $item->value);
      $query->condition($or);

      $value_taken = (bool) $query
        ->range(0, 1)
        ->count()
        ->execute();

      if ($value_taken) {
        $is_unique = FALSE;
        $bad_email = $item->value;
        break;
      }
    }
    if (!$is_unique) {
      $this->context->addViolation($constraint->notUnique, [
        '%value' => $bad_email,
      ]);
    }

  }

}
