<?php

declare(strict_types=1);

namespace Drupal\Tests\alternative_user_emails\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\user\UserInterface;

/**
 * Tests alternative_user_emails user email validation.
 *
 * @group alternative_user_emails
 */
class AlternativeUserEmailsFieldTest extends EntityKernelTestBase {
  /**
   * {@inheritdoc}
   */
  protected static $modules = ['user', 'system', 'alternative_user_emails'];

  /**
   * {@inheritdoc}
   */
  #[\Override]
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
  }

  /**
   * Test that when the primary is changed the old primary becomes an alternate.
   */
  public function testOldPrimariesBecomeAlternates(): void {
    // Create a user with a primary email and no alternates.
    $user = $this->createUser([], 'user1', FALSE, ['mail' => '1@example.com']);
    $this->assertUserValid($user);
    $user->save();
    $this->assertUserEmail($user, '1@example.com');
    $this->assertUserEmailAlternates($user, []);
    $this->assertUserValid($user);

    // Change the user email, so the old primary becomes an alternate.
    $user->setEmail('2@example.com');
    $this->assertUserValid($user);
    $user->save();
    $this->assertUserEmail($user, '2@example.com');
    $this->assertUserEmailAlternates($user, ['1@example.com']);
    $this->assertUserValid($user);

    // Make the old primary the primary again.
    $user->setEmail('1@example.com');
    $this->assertUserValid($user);
    $user->save();
    $this->assertUserEmail($user, '1@example.com');
    $this->assertUserEmailAlternates($user, ['2@example.com']);
    $this->assertUserValid($user);

    // Add a new primary.
    $user->setEmail('3@example.com');
    $this->assertUserValid($user);
    $user->save();
    $this->assertUserEmail($user, '3@example.com');
    $this->assertUserEmailAlternates($user, ['2@example.com', '1@example.com']);
    $this->assertUserValid($user);

    // Make an alternate into the primary again.
    $user->setEmail('2@example.com');
    $this->assertUserValid($user);
    $user->save();
    $this->assertUserEmail($user, '2@example.com');
    $this->assertUserEmailAlternates($user, ['1@example.com', '3@example.com']);
    $this->assertUserValid($user);

  }

  /**
   * Test that unique validation is enforced on mail and alternatives.
   *
   * @dataProvider validationProvider
   */
  public function testValidationWithAlternatives(array $user1Mails, array $user2Mails, string $expectation): void {
    $user1Alternates = [];
    foreach ($user1Mails['alternates'] as $alternate) {
      $user1Alternates[] = ['value' => $alternate];
    }
    $user2Alternates = [];
    foreach ($user2Mails['alternates'] as $alternate) {
      $user2Alternates[] = ['value' => $alternate];
    }

    $user1 = $this->createUser([], 'user1', FALSE, [
      'mail' => $user1Mails['primary'],
      'alternative_user_emails' => $user1Alternates,
    ]);
    $user1->save();
    $this->assertUserEmailAlternates($user1, $user1Mails['alternates']);

    $user2 = $this->createUser([], 'user2', FALSE, [
      'mail' => $user2Mails['primary'],
      'alternative_user_emails' => $user2Alternates,
    ]);

    if ($expectation === 'valid') {
      $this->assertUserValid($user2);
    }
    else {
      $this->assertUserInvalid($user2);
    }
  }

  /**
   * Data provider for testValidationWithAlternatives.
   *
   * @return array
   *   The scenarios to test.
   */
  public static function validationProvider(): array {
    return [
      'primary clash' => [
        [
          'primary' => '1@example.com',
          'alternates' => [],
        ],
        [
          'primary' => '1@example.com',
          'alternates' => [],
        ],
        'invalid',
      ],
      'primaries only no clash' => [
        [
          'primary' => '1@example.com',
          'alternates' => [],
        ],
        [
          'primary' => '2@example.com',
          'alternates' => [],
        ],
        'valid',
      ],
      'primary and alternates no clash' => [
        [
          'primary' => '1@example.com',
          'alternates' => ['3@example.com'],
        ],
        [
          'primary' => '2@example.com',
          'alternates' => ['4@example.com'],
        ],
        'valid',
      ],
      'multiple alternates no clash' => [
        [
          'primary' => '1@example.com',
          'alternates' => ['2@example.com', '3@example.com'],
        ],
        [
          'primary' => '4@example.com',
          'alternates' => ['5@example.com', '6@example.com'],
        ],
        'valid',
      ],
      'primary clash with alternate' => [
        [
          'primary' => '1@example.com',
          'alternates' => ['2@example.com'],
        ],
        [
          'primary' => '2@example.com',
          'alternates' => [],
        ],
        'invalid',
      ],
      'primary clash with first alternate' => [
        [
          'primary' => '1@example.com',
          'alternates' => ['2@example.com', '3@example.com'],
        ],
        [
          'primary' => '2@example.com',
          'alternates' => [],
        ],
        'invalid',
      ],
      'primary clash with second alternate' => [
        [
          'primary' => '1@example.com',
          'alternates' => ['2@example.com', '3@example.com'],
        ],
        [
          'primary' => '3@example.com',
          'alternates' => [],
        ],
        'invalid',
      ],
      'primary clash with alternate but alternate does not' => [
        [
          'primary' => '1@example.com',
          'alternates' => ['2@example.com'],
        ],
        [
          'primary' => '2@example.com',
          'alternates' => ['3@example.com'],
        ],
        'invalid',
      ],
      'alternate clash with primary without alternates' => [
        [
          'primary' => '1@example.com',
          'alternates' => [],
        ],
        [
          'primary' => '2@example.com',
          'alternates' => ['1@example.com'],
        ],
        'invalid',
      ],
      'alternate clash with primary not alternates' => [
        [
          'primary' => '1@example.com',
          'alternates' => ['3@example.com'],
        ],
        [
          'primary' => '2@example.com',
          'alternates' => ['1@example.com'],
        ],
        'invalid',
      ],
      'alternate clash with alternate' => [
        [
          'primary' => '1@example.com',
          'alternates' => ['3@example.com'],
        ],
        [
          'primary' => '2@example.com',
          'alternates' => ['3@example.com'],
        ],
        'invalid',
      ],
      'alternate clash with first alternate' => [
        [
          'primary' => '1@example.com',
          'alternates' => ['3@example.com', '4@example.com'],
        ],
        [
          'primary' => '2@example.com',
          'alternates' => ['3@example.com'],
        ],
        'invalid',
      ],
      'alternate clash with second alternate' => [
        [
          'primary' => '1@example.com',
          'alternates' => ['3@example.com', '4@example.com'],
        ],
        [
          'primary' => '2@example.com',
          'alternates' => ['4@example.com'],
        ],
        'invalid',
      ],
      'first alternate clash with primary' => [
        [
          'primary' => '1@example.com',
          'alternates' => [],
        ],
        [
          'primary' => '2@example.com',
          'alternates' => ['1@example.com', '3@example.com'],
        ],
        'invalid',
      ],
      'second alternate clash with primary' => [
        [
          'primary' => '1@example.com',
          'alternates' => [],
        ],
        [
          'primary' => '2@example.com',
          'alternates' => ['3@example.com', '1@example.com'],
        ],
        'invalid',
      ],

    ];
  }

  /**
   * Perform validation on a user.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user to validate.
   *
   * @return array
   *   An array of string validation messages.
   */
  protected function getUserValidation(UserInterface $user): array {
    $violations = $user->validate();
    $messages = [];
    foreach ($violations as $violation) {
      $messages[] = (string) $violation->getMessage();
    }
    return $messages;
  }

  /**
   * Assert that a user passes entity validation.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   */
  protected function assertUserValid(UserInterface $user): void {
    $validation = $this->getUserValidation($user);
    $this::assertEmpty($validation, "There should be no validation errors, but there were:" . print_r($validation, TRUE));
  }

  /**
   * Assert that a user fails entity validation.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   */
  protected function assertUserInvalid(UserInterface $user): void {
    $validation = $this->getUserValidation($user);
    $this::assertNotEmpty($validation, "There should be validation errors");
  }

  /**
   * Assert that a user has particular alternative emails.
   *
   * @param \Drupal\user\UserInterface $user
   *   The actual user.
   * @param array $expected
   *   An array of expected emails.
   */
  protected function assertUserEmailAlternates(UserInterface $user, array $expected): void {
    $value = $user->get('alternative_user_emails')->getValue();
    $actual = array_column($value, 'value');
    $this::assertEquals($expected, $actual, "Alternative emails should be stored");
  }

  /**
   * Assert that a user has a particular email.
   *
   * @param \Drupal\user\UserInterface $user
   *   The actual user.
   * @param string $expected
   *   The expected email.
   */
  protected function assertUserEmail(UserInterface $user, string $expected): void {
    $this::assertEquals($expected, $user->getEmail(), "Primary email expected to be " . $expected);
  }

}
