<?php

declare(strict_types=1);

namespace Drupal\Tests\alternative_user_emails\Kernel;

use Drupal\Core\Entity\Query\ConditionInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\user\Entity\User;

/**
 * Tests alternative_user_emails user entity query.
 *
 * @group alternative_user_emails
 */
class AlternativeUserEmailsQueryTest extends EntityKernelTestBase {
  /**
   * The entity query object to test.
   *
   * @var \Drupal\Core\Entity\Query\QueryInterface
   */
  protected QueryInterface $query;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['user', 'system', 'alternative_user_emails'];

  /**
   * {@inheritdoc}
   */
  #[\Override]
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
  }

  /**
   * Test that user queries not needing alteration are still working.
   */
  public function testBasicUserQuery(): void {
    $john = $this->createUser([], 'john', FALSE, [
      'mail' => 'john@example.com',
    ]);
    $john->save();
    $jane = $this->createUser([], 'jane', FALSE, [
      'mail' => 'jane@example.com',
    ]);
    $jane->save();
    $fred = $this->createUser([], 'fred', FALSE, [
      'mail' => 'fred@example.com',
    ]);
    $fred->save();

    $this->query = \Drupal::entityQuery('user')
      ->accessCheck(FALSE)
      ->condition('name', ['john', 'fred'], 'IN');
    $this->assertResults([$john, $fred]);

    $this->query = \Drupal::entityQuery('user')
      ->accessCheck(FALSE);
    $or = $this->query->orConditionGroup()
      ->condition('name', 'john')
      ->condition('name', 'fred');
    $this->query->condition($or);
    $this->assertResults([$john, $fred]);

    $this->query = \Drupal::entityQuery('user')
      ->accessCheck(FALSE)
      ->condition('mail', ['john@example.com', 'fred@example.com'], 'IN');
    $this->assertResults([$john, $fred]);

    $this->query = \Drupal::entityQuery('user')
      ->accessCheck(FALSE);
    $or = $this->query->orConditionGroup()
      ->condition('mail', 'john@example.com')
      ->condition('name', 'fred');
    $this->query->condition($or);
    $this->assertResults([$john, $fred]);

    // When querying 3 users,
    // 2 are caught by one condition,
    // 2 are caught by an or group,
    // but only 1 is caught by both the condition + or group.
    $this->query = \Drupal::entityQuery('user')
      ->accessCheck(FALSE)
      ->condition('name', ['john', 'fred'], 'IN');
    $or = $this->query->orConditionGroup();
    $or->condition('mail', 'john@example.com');
    $or->condition('mail', 'jane@example.com');
    $this->query->condition($or);
    $this->assertResults([$john]);
  }

  /**
   * Test that alternative emails are considered when querying users by mail.
   */
  public function testAlternatesUserQuery(): void {

    $john = $this->createUser([], 'john', FALSE, [
      'mail' => 'john@example.com',
      'alternative_user_emails' => 'john2@example.com',
    ]);
    $john->save();
    $jane = $this->createUser([], 'jane', FALSE, [
      'mail' => 'jane@example.com',
    ]);
    $jane->save();
    $fred = $this->createUser([], 'fred', FALSE, [
      'mail' => 'fred@example.com',
    ]);
    $fred->save();

    // Still possible to match against mail despite alternate.
    $this->query = \Drupal::entityQuery('user')
      ->accessCheck(FALSE)
      ->condition('mail', 'john@example.com');
    $this->assertResults([$john]);

    // Still matches against mail for multiple users despite alternate.
    $this->query = \Drupal::entityQuery('user')
      ->accessCheck(FALSE);
    $or = $this->query->orConditionGroup()
      ->condition('mail', 'john@example.com')
      ->condition('mail', 'fred@example.com');
    $this->query->condition($or);
    $this->assertResults([$john, $fred]);

    // Mail searches against alternates automatically.
    $this->query = \Drupal::entityQuery('user')
      ->accessCheck(FALSE)
      ->condition('mail', 'john2@example.com');
    $this->assertResults([$john]);

    // Operator is carried over into searches against alternates.
    $this->query = \Drupal::entityQuery('user')
      ->accessCheck(FALSE)
      ->condition('mail', 'john2', 'CONTAINS');
    $this->assertResults([$john]);

    // Search against mail and alternates simultaneously.
    $this->query = \Drupal::entityQuery('user')
      ->accessCheck(FALSE);
    $or = $this->query->orConditionGroup()
      ->condition('mail', 'john2@example.com')
      ->condition('mail', 'fred@example.com');
    $this->query->condition($or);
    $this->assertResults([$john, $fred]);

    // Search against alternate and another field.
    $this->query = \Drupal::entityQuery('user')
      ->accessCheck(FALSE)
      ->condition('mail', 'john2@example.com')
      ->condition('name', 'john');
    $this->assertResults([$john]);

    // Search against alternate and another field using or group.
    // Also test a contains operator.
    $this->query = \Drupal::entityQuery('user')
      ->accessCheck(FALSE);
    $or = $this->query->orConditionGroup()
      ->condition('mail', 'john2', 'CONTAINS')
      ->condition('name', 'fred');
    $this->query->condition($or);
    $this->assertResults([$john, $fred]);

    // Test <> operator on mail.
    $this->query = \Drupal::entityQuery('user')
      ->accessCheck(FALSE)
      ->condition('mail', 'john@example.com', '<>')
      ->condition('name', 'john');
    $this->assertResults([]);

    // Test <> operator on alternative.
    $this->query = \Drupal::entityQuery('user')
      ->accessCheck(FALSE)
      ->condition('mail', 'john2@example.com', '<>')
      ->condition('name', 'john');
    $this->assertResults([]);

    // Test NOT IN operator on mail.
    $this->query = \Drupal::entityQuery('user')
      ->accessCheck(FALSE)
      ->condition('mail', ['john@example.com', 'fred@example.com'], 'NOT IN')
      ->condition('name', 'john');
    $this->assertResults([]);

    // Test NOT IN  operator on alternative.
    $this->query = \Drupal::entityQuery('user')
      ->accessCheck(FALSE)
      ->condition('mail', ['john@example.com', 'fred@example.com'], 'NOT IN')
      ->condition('name', 'john');
    $this->assertResults([]);
  }

  /**
   * Assert that the user query found specific users.
   *
   * @param array $expected
   *   The users the query is expected to find.
   *
   * @throws \ReflectionException
   */
  protected function assertResults(array $expected): void {
    $results = $this->query->execute();

    $expectedUsers = [];
    foreach ($expected as $user) {
      $expectedUsers[$user->id()] = $user->getAccountName();
    }
    $actualUsers = [];
    foreach ($results as $userId) {
      $user = User::load($userId);
      $actualUsers[$user->id()] = $user->getAccountName();
    }
    sort($expectedUsers);
    sort($actualUsers);

    $message = $this->conditionsMessage();
    $this::assertEquals($expectedUsers, $actualUsers, $message);
  }

  /**
   * Generate a test failure message that includes the query conditions.
   *
   * @return string
   *   A message.
   *
   * @throws \ReflectionException
   */
  protected function conditionsMessage(): string {
    $reflection = new \ReflectionClass($this->query);
    $property = $reflection->getProperty('condition');
    $property->setAccessible(TRUE);
    $conditionsObject = $property->getValue($this->query);
    $conditions = $conditionsObject->conditions();
    $conditions = $this->prettifyConditions($conditions);
    return "Unexpected users returned from query with these conditions:" . print_r($conditions, TRUE);
  }

  /**
   * Convert an array of query conditions into a pretty array for printing.
   *
   * This is just for debugging and troubleshooting.
   *
   * @param array &$conditions
   *   An array output by \Drupal\Core\Entity\QueryInterface::conditions()
   *
   * @return array
   *   A (possibly nested) array of strings.
   */
  protected function prettifyConditions(array &$conditions): array {
    foreach ($conditions as &$condition) {
      if (isset($condition['field'])) {
        $field = $condition['field'];
        if ($field instanceof ConditionInterface) {
          $condition = [
            // These array keys are arbitrary, just for pretty printing.
            'conjunction' => $field->getConjunction(),
            'group' => $this->prettifyConditions($field->conditions()),
          ];
        }
      }
    }
    return $conditions;
  }

}
