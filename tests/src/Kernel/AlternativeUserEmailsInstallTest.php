<?php

declare(strict_types=1);

namespace Drupal\Tests\alternative_user_emails\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test installation and uninstallation.
 *
 * @group alternative_user_emails
 */
class AlternativeUserEmailsInstallTest extends KernelTestBase {

  /**
   * Test that the module can be installed and uninstalled.
   */
  public function testInstallUninstall(): void {
    \Drupal::service('module_installer')->install(['alternative_user_emails']);
    $userFields = \Drupal::service('entity_field.manager')->getFieldDefinitions('user', 'user');
    $this::assertTrue(isset($userFields['alternative_user_emails']));

    \Drupal::service('module_installer')->uninstall(['alternative_user_emails']);
    $userFields = \Drupal::service('entity_field.manager')->getFieldDefinitions('user', 'user');
    $this::assertFalse(isset($userFields['alternative_user_emails']));

    \Drupal::service('module_installer')->install(['alternative_user_emails']);
    $userFields = \Drupal::service('entity_field.manager')->getFieldDefinitions('user', 'user');
    $this::assertTrue(isset($userFields['alternative_user_emails']));

    \Drupal::service('module_installer')->uninstall(['alternative_user_emails']);
    $userFields = \Drupal::service('entity_field.manager')->getFieldDefinitions('user', 'user');
    $this::assertFalse(isset($userFields['alternative_user_emails']));
  }

}
